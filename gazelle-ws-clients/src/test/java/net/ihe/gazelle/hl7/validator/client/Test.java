package net.ihe.gazelle.hl7.validator.client;

import net.ihe.gazelle.hl7.validator.client.HL7v2Validator;
import net.ihe.gazelle.hl7.validator.client.MessageEncoding;
import org.junit.Ignore;

import java.io.IOException;

@Ignore
public class Test {

	private static final String ENDPOINT = "http://localhost:8080/GazelleHL7v2Validator-ejb/gazelleHL7v2ValidationWSService/gazelleHL7v2ValidationWS?wsdl";
	private static final String PROFILE_OID = "1.3.6.1.4.12559.11.1.1.178";

	public static void main(String[] args) throws IOException {
		String profileOid = PROFILE_OID;
		String message = "MSH|^~\\&|ABBOTT_IVD|ABBOTT|OM_LAB_ANALYZER_MGR|IHE|20130719173135||QBP^Q11^QBP_Q11|627ae01c-7c5c-428e-98d4-ddc035954671|P|2.5.1|||ER|AL||UNICODE UTF-8|||LAB-27^IHE\r"
				+ "QPD|WOS^Work Order Step^IHE_LABTF|QRY-0001|PATIENT-0001\r" + "RCP|I||R\r";
		HL7v2Validator validator = new HL7v2Validator(ENDPOINT);
		String result = validator.validate(message, profileOid, "UTF-8", MessageEncoding.ER7);
		System.out.println(result);
		System.out.print("status: " + validator.getLastValidationStatus());
		System.out.print("#errors: " + validator.getErrorsCountForLastValidation());
		System.out.print("#warnings: " + validator.getWarningsCountForLastValidation());
	}
}
