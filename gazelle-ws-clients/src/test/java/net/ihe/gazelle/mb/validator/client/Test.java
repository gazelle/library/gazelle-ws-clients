package net.ihe.gazelle.mb.validator.client;

import net.ihe.gazelle.mb.validator.client.MBValidator;
import net.ihe.gazelle.mb.validator.client.ServiceConnectionError;
import net.ihe.gazelle.mb.validator.client.ServiceOperationError;
import org.junit.Ignore;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.List;

@Ignore
public class Test {
	
	private static final String ENDPOINT = "http://jumbo.irisa.fr:8080/SVSSimulator-SVSSimulator-ejb/ModelBasedValidation?wsdl";
	private static final String PATH_TO_FILE = "/opt/SVSRepository_sake/1.3.6.1.4.1.21367.101.101.xml";

	/**
	 * @param args
	 */
	public static void main(String[] args) throws ServiceConnectionError, ServiceOperationError {
		File file = new File(PATH_TO_FILE);
		String document = null;
		try{
			FileInputStream fis = new FileInputStream(file);
			byte[] buffer = new byte[(int) file.length()];
			fis.read(buffer);
			document = new String(buffer);
		}catch(FileNotFoundException e){
			System.out.println("Cannot find file at " + PATH_TO_FILE);
		} catch (IOException e) {
			System.out.println("Cannot read file");
		}
		if (document != null && !document.isEmpty())
		{
			MBValidator validator = new MBValidator(ENDPOINT);
			List<String> validators = validator.listAvailableValidators(null);
			if (validators != null)
			{
				for (String validatorName : validators)
					System.out.println(validatorName);
				
				String result = validator.validate(document, validators.get(0), false);
				System.out.println("result: " + result);
			}
		}
	}

}
