package net.ihe.gazelle.chiua.validator.client;

import net.ihe.gazelle.app.iti71validator.ws.QueryValidator;
import net.ihe.gazelle.app.iti71validator.ws.QueryValidatorE;
import net.ihe.gazelle.chiua.ws.ITI71ValidatorWSImplServiceStub;
import net.ihe.gazelle.validator.dcc.*;
import org.apache.axis2.AxisFault;
import org.apache.axis2.transport.http.HTTPConstants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.rmi.RemoteException;

public class ITI71Validator {
    private static final long DEFAULT_TIMEOUT = 60000;


    private static Logger log = LoggerFactory.getLogger(ITI71Validator.class);

    /**
     * Target endpoint to contact the model based validator (must extends AbstractModelBasedValidator
     */
    protected String endpoint;
    /**
     * Detailed validation result returned by the simulator
     */
    protected String validationResult;
    /**
     * Stub to contact validator
     */

    protected ITI71ValidatorWSImplServiceStub stub;
    /**
     * Timeout (in millisecond) on client side when waiting for server response
     */
    protected long timeout;

    /**
     * Constructor
     *
     * @param endpoint : string endpoint
     */
    public ITI71Validator(String endpoint) {
        this.endpoint = endpoint;
        this.stub = null;
        this.timeout = DEFAULT_TIMEOUT;
    }

    public ITI71Validator(String endpoint, long timeout) {
        this.endpoint = endpoint;
        this.stub = null;
        this.timeout = timeout;
    }

    private void getStub() throws AxisFault {
        if (stub == null) {
            stub = new ITI71ValidatorWSImplServiceStub(this.endpoint);
            stub._getServiceClient().getOptions().setProperty(org.apache.axis2.Constants.Configuration.DISABLE_SOAP_ACTION, true);
            stub._getServiceClient().getOptions().setProperty(HTTPConstants.SO_TIMEOUT, (int) timeout);
            stub._getServiceClient().getOptions().setProperty(HTTPConstants.CONNECTION_TIMEOUT, (int) timeout);
        }
    }

    /**
     * Validate the document against the given validator
     *
     * @return the detailed result as forwarded by the validator
     */
    public String validate(String objectToValidate, String url, Boolean isExtended, String accept, String contentType, String authorization, String jwt) throws UnkownValidatorExceptionException {
        this.validationResult = null;

        if (objectToValidate == null) {
            log.error("The objectToValidate is either null or empty");
        } else {
            try {
                getStub();
                QueryValidator queryValidator = new QueryValidator();
                queryValidator.setObjectToValidate(objectToValidate);
                queryValidator.setUrl(url);
                queryValidator.setIsExtended(isExtended);
                queryValidator.setAccept(accept);
                queryValidator.setContentType(contentType);
                queryValidator.setAuthorization(authorization);
                queryValidator.setJwt(jwt);
                QueryValidatorE queryValidatorE = new QueryValidatorE();
                queryValidatorE.setQueryValidator(queryValidator);

                this.validationResult = stub.queryValidator(queryValidatorE).getQueryValidatorResponse().getDetailedResult();

            } catch (AxisFault e) {
                log.error("a", e);
                log.error("An error occurred at validation time: " + e.getMessage());
            } catch (RemoteException e) {
                log.error("An error occurred at validation time: " + e.getMessage());
            }
        }
        return validationResult;
    }


    public long getTimeout() {
        return timeout;
    }

    public void setTimeout(long timeout) {
        this.timeout = timeout;
    }
}
