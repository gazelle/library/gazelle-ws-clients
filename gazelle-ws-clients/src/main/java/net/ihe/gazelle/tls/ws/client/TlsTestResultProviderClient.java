package net.ihe.gazelle.tls.ws.client;

import net.ihe.gazelle.preferences.PreferenceService;
import org.jboss.resteasy.client.ClientExecutor;
import org.jboss.resteasy.client.ClientRequest;
import org.jboss.resteasy.client.ClientResponse;
import org.jboss.resteasy.client.core.executors.ApacheHttpClient4Executor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.Response.Status;
import java.util.List;

public class TlsTestResultProviderClient {

    private static final String HD_WARNING = "Warning";
    private static final ClientExecutor CLIENT_EXECUTOR = new ApacheHttpClient4Executor();

    private static Logger log = LoggerFactory.getLogger(TlsTestResultProviderClient.class);

    public TlsTestResultProviderClient() {

    }

    protected String getTLSApplicationUrl() {
        return PreferenceService.getString("gazelle_tls_url");
    }

    public String getTlsConnectionStatus(String id) {
        return getValue("/rest/testResult/connection/{id}", id);
    }

    public String getTlsTestInstanceStatus(String id) {
        return getValue("/rest/testResult/instance/{id}", id);
    }

    private String getValue(String template, String id) {
        String baseUrl = getTLSApplicationUrl();
        if (id != null && baseUrl != null) {
            StringBuilder uriTemplate = new StringBuilder(baseUrl);
            uriTemplate.append(template);
            ClientRequest request = new ClientRequest(uriTemplate.toString(), CLIENT_EXECUTOR);
            request.pathParameter("id", id);
            try {
                ClientResponse<String> response = request.get(String.class);
                if (response.getStatus() != Status.OK.getStatusCode()) {
                    MultivaluedMap<String, String> headers = response.getHeaders();
                    List<String> warnings = headers.get(HD_WARNING);
                    if (warnings != null && !warnings.isEmpty()) {
                        log.error(warnings.get(0));
                    } else {
                        log.error("Service returned code " + response.getStatus());
                    }
                    response.releaseConnection();
                    return null;
                } else {
                    String entity = response.getEntity();
                    response.releaseConnection();
                    return entity;
                }
            } catch (Exception e) {
                log.error(e.getMessage(), e);
                return null;
            }
        } else {
            log.error("Both id and TLS application URL are required");
            return null;
        }
    }

    public static void main(String[] args) {
        TlsTestResultProviderClient client = new TlsTestResultProviderClient() {
            @Override
            protected String getTLSApplicationUrl() {
                return "http://localhost:8080/atna";
            }
        };
        String result = client.getTlsConnectionStatus("164");
        if (result != null) {
            System.out.println("returned TLS result: " + result);
        }

        result = client.getTlsTestInstanceStatus("60");
        if (result != null) {
            System.out.println("return instance status: " + result);
        }
    }
}
