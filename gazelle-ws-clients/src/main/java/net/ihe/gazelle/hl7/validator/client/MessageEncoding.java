package net.ihe.gazelle.hl7.validator.client;


public enum MessageEncoding {
	XML("XML"),
	ER7("ER7");
	
	String value;
	
	MessageEncoding(String value){
		this.value = value;
	}
	
	public String getValue(){
		return this.value;
	}
}
