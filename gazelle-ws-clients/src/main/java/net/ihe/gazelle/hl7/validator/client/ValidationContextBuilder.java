package net.ihe.gazelle.hl7.validator.client;

import org.xml.sax.SAXException;

import javax.xml.XMLConstants;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.sax.SAXTransformerFactory;
import javax.xml.transform.sax.TransformerHandler;
import javax.xml.transform.stream.StreamResult;
import java.io.ByteArrayOutputStream;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

public class ValidationContextBuilder {

    public static final String MESSAGE_STRUCTURE = "MessageStructure";
    public static final String LENGTH = "Length";
    public static final String DATATYPE = "DataType";
    public static final String DATAVALUE = "DataValue";
    private static final String ROOT_ELEMENT = "ValidationContext";
    private static final String PROFILE_OID_ELEMENT = "ProfileOID";
    private static final String VALIDATION_OPTIONS_ELEMENT = "ValidationOptions";
    private static final String CHARACTER_ENCODING_ELEMENT = "CharacterEncoding";
    private static Logger log = Logger.getLogger("net.ihe.gazelle.hl7.validator.client.ValidationContextBuilder");
    protected String profileOid;
    protected String encodingCharacter;
    /**
     * this map represents the validation options. The key is the qName of the option element (Length for example) and the value is the level of fault to report (error, warning ...)
     */
    protected Map<String, FaultLevel> options;

    public ValidationContextBuilder(String profileOid, String encodingCharacter, Map<String, FaultLevel> options) {
        this.profileOid = profileOid;
        this.encodingCharacter = encodingCharacter;
        this.options = options;
    }

    public String build() {
        try {
            // initiate the transformer
            SAXTransformerFactory factory = (SAXTransformerFactory) SAXTransformerFactory.newInstance(
                    "com.sun.org.apache.xalan.internal.xsltc.trax.TransformerFactoryImpl",
                    ClassLoader.getSystemClassLoader());
            TransformerHandler handler = factory.newTransformerHandler();
            handler.getTransformer().setOutputProperty(OutputKeys.INDENT, "yes");
            // transformation output will be stored in result
            StreamResult result = new StreamResult(new ByteArrayOutputStream());
            handler.setResult(result);

            // start XML
            handler.startDocument();
            // root
            handler.startElement(XMLConstants.NULL_NS_URI, "", ROOT_ELEMENT, null);

            // profile OID
            handler.startElement("", "", PROFILE_OID_ELEMENT, null);
            handler.characters(profileOid.toCharArray(), 0, profileOid.length());
            handler.endElement("", "", PROFILE_OID_ELEMENT);

            // validation options
            handler.startElement("", "", VALIDATION_OPTIONS_ELEMENT, null);
            for (String key : options.keySet()) {
                handler.startElement("", "", key, null);
                String level = options.get(key).levelAsString;
                handler.characters(level.toCharArray(), 0, level.length());
                handler.endElement("", "", key);
            }
            handler.endElement("", "", VALIDATION_OPTIONS_ELEMENT);

            // encoding character
            if (encodingCharacter != null && !encodingCharacter.isEmpty()) {
                handler.startElement("", "", CHARACTER_ENCODING_ELEMENT, null);
                handler.characters(encodingCharacter.toCharArray(), 0, encodingCharacter.length());
                handler.endElement("", "", CHARACTER_ENCODING_ELEMENT);
            }

            // end XML
            handler.endElement("", "", ROOT_ELEMENT);
            handler.endDocument();

            ByteArrayOutputStream baos = (ByteArrayOutputStream) result.getOutputStream();
            return new String(baos.toByteArray());
        } catch (TransformerConfigurationException e) {
            log.log(Level.SEVERE, e.getMessage());
        } catch (SAXException e) {
            log.log(Level.SEVERE, e.getMessage());
            e.printStackTrace();
        }
        return null;
    }

    public enum FaultLevel {
        ERROR("ERROR"),
        WARNING("WARNING"),
        IGNORE("IGNORE");

        String levelAsString;

        FaultLevel(String levelString) {
            this.levelAsString = levelString;
        }

        /**
         * Returns the enum the levelAsString of which matches the inLabel parameter default is WARNING if none matches
         *
         * @param inLabel : level in string
         * @return the enum the levelAsString
         */
        public static FaultLevel getFaultLevelByLabel(String inLabel) {
            if (inLabel != null) {
                for (FaultLevel level : FaultLevel.values()) {
                    if (level.getLevelAsString().equalsIgnoreCase(inLabel)) {
                        return level;
                    } else {
                        continue;
                    }
                }
            }
            return WARNING;
        }

        public String getLevelAsString() {
            return levelAsString;
        }
    }
}
