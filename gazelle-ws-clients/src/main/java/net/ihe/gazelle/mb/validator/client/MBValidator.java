package net.ihe.gazelle.mb.validator.client;

import java.io.BufferedOutputStream;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;
import java.util.zip.ZipOutputStream;

import javax.xml.bind.DatatypeConverter;

import org.apache.axis2.AxisFault;
import org.apache.axis2.transport.http.HTTPConstants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import net.ihe.gazelle.mb.validator.ModelBasedValidationWSServiceStub;
import net.ihe.gazelle.mb.validator.SOAPExceptionException;
import net.ihe.gazelle.validator.mb.ws.GetListOfValidators;
import net.ihe.gazelle.validator.mb.ws.GetListOfValidatorsE;
import net.ihe.gazelle.validator.mb.ws.ValidateB64Document;
import net.ihe.gazelle.validator.mb.ws.ValidateB64DocumentE;
import net.ihe.gazelle.validator.mb.ws.ValidateB64DocumentResponseE;
import net.ihe.gazelle.validator.mb.ws.ValidateBase64Document;
import net.ihe.gazelle.validator.mb.ws.ValidateBase64DocumentE;
import net.ihe.gazelle.validator.mb.ws.ValidateBase64DocumentResponseE;
import net.ihe.gazelle.validator.mb.ws.ValidateDocument;
import net.ihe.gazelle.validator.mb.ws.ValidateDocumentE;
import net.ihe.gazelle.validator.mb.ws.ValidateDocumentResponseE;

public class MBValidator {

   private static final long DEFAULT_TIMEOUT = 60000;


   private static Logger log = LoggerFactory.getLogger(MBValidator.class);

   /**
    * Target endpoint to contact the model based validator (must extends AbstractModelBasedValidator
    */
   protected String endpoint;
   /**
    * Detailed validation result returned by the simulator
    */
   protected String validationResult;
   /**
    * Stub to contact validator
    */
   protected ModelBasedValidationWSServiceStub stub;
   /**
    * Timeout (in millisecond) on client side when waiting for server response
    */
   protected long timeout;

   /**
    * Constructor
    *
    * @param endpoint : string endpoint
    */
   public MBValidator(String endpoint) {
      this.endpoint = endpoint;
      this.stub = null;
      this.timeout = DEFAULT_TIMEOUT;
   }

   public MBValidator(String endpoint, long timeout) {
      this.endpoint = endpoint;
      this.stub = null;
      this.timeout = timeout;
   }

   /**
    * Returns the list of validators available on the contacted service side
    *
    * @param descriminator string to filter validator list
    *
    * @return list of validators available filtered by the given descriminator.
    *
    * @throws ServiceConnectionError if this client cannot connect at the given service URL.
    * @throws ServiceOperationError  if an error occurs during the remote operation call.
    */
   public List<String> listAvailableValidators(String descriminator) throws ServiceConnectionError, ServiceOperationError {
      try {
         getStub();
         GetListOfValidators params = new GetListOfValidators();
         params.setDescriminator(descriminator);
         GetListOfValidatorsE paramsE = new GetListOfValidatorsE();
         paramsE.setGetListOfValidators(params);
         String[] validators = stub.getListOfValidators(paramsE).getGetListOfValidatorsResponse().getValidators();
         return validators != null ? Arrays.asList(validators) : new ArrayList<String>();
      } catch (AxisFault e) {
         throw new ServiceConnectionError("Unable to connect to the validation service at " + this.endpoint, e);
      } catch (RemoteException e) {
         throw new ServiceOperationError("Cannot retrieve the list of validators: " + e.getMessage(), e);
      } catch (SOAPExceptionException e) {
         throw new ServiceOperationError("Cannot retrieve the list of validators: " + e.getFaultMessage().getSOAPException().getMessage(), e);
      }
   }

   private void getStub() throws AxisFault {
      if (stub == null) {
         stub = new ModelBasedValidationWSServiceStub(this.endpoint);
         stub._getServiceClient().getOptions().setProperty(org.apache.axis2.Constants.Configuration.DISABLE_SOAP_ACTION, true);
         stub._getServiceClient().getOptions().setProperty(HTTPConstants.SO_TIMEOUT, (int) timeout);
         stub._getServiceClient().getOptions().setProperty(HTTPConstants.CONNECTION_TIMEOUT, (int) timeout);
      }
   }

   /**
    * Validate the document against the given validator
    *
    * @param document:         the document to validate (plain text or base64 encoded)
    * @param validator:        the validator to use
    * @param isDocumentBase64: indicates if the document is base64 encoded or not
    *
    * @return the detailed result as forwarded by the validator
    *
    * @throws ServiceConnectionError if this client cannot connect at the given service URL.
    * @throws ServiceOperationError  if an error occurs during the remote operation call.
    */
   public String validate(String document, String validator, boolean isDocumentBase64) throws ServiceConnectionError, ServiceOperationError {
      this.validationResult = null;
      if (document == null || document.isEmpty()) {
         log.error("The document to validate is either null or empty");
      } else if (validator == null || validator.isEmpty()) {
         log.error("The validator to call is either null or empty");
      } else {
         try {
            getStub();
            if (isDocumentBase64) {
               ValidateBase64Document params = new ValidateBase64Document();
               params.setBase64Document(document);
               params.setValidator(validator);
               ValidateBase64DocumentE paramsE = new ValidateBase64DocumentE();
               paramsE.setValidateBase64Document(params);
               ValidateBase64DocumentResponseE responseE = stub.validateBase64Document(paramsE);
               this.validationResult = responseE.getValidateBase64DocumentResponse().getDetailedResult();
            } else {
               ValidateDocument params = new ValidateDocument();
               params.setDocument(document);
               params.setValidator(validator);
               ValidateDocumentE paramsE = new ValidateDocumentE();
               paramsE.setValidateDocument(params);
               ValidateDocumentResponseE responseE = stub.validateDocument(paramsE);
               this.validationResult = responseE.getValidateDocumentResponse().getDetailedResult();
            }
         } catch (AxisFault e) {
            throw new ServiceConnectionError("Unable to connect to the validation service at " + this.endpoint, e);
         } catch (RemoteException e) {
            throw new ServiceOperationError("An error occured during validation operation call: " + e.getMessage(), e);
         } catch (SOAPExceptionException e) {
            throw new ServiceOperationError(
                  "An error occured during validatio operation call: " + e.getFaultMessage().getSOAPException().getMessage(), e);
         }
      }
      return validationResult;
   }

   /**
    * Validate the document against the given validator
    *
    * @param documentB64: the document zipped and encoded
    * @param validator:   the validator to use
    * @param mediaType:   the mediatype of the result of validation
    *
    * @return the detailed result as forwarded by the validator
    */
   public String validateZipAndReturnB64Zip(String documentB64, String validator, String mediaType) {
      try {
         getStub();
         ValidateB64Document params = new ValidateB64Document();
         params.setBase64Document(documentB64);
         params.setValidator(validator);
         params.setResultMediaType("application/zip");
         ValidateB64DocumentE paramsE = new ValidateB64DocumentE();
         paramsE.setValidateB64Document(params);
         ValidateB64DocumentResponseE responseE = stub.validateB64Document(paramsE);
         return responseE.getValidateB64DocumentResponse().getDetailedResult();
      } catch (Exception e) {
         log.error("Problem to validate the B64 doc");
      }
      return null;
   }

   /**
    * Validate the document against the given validator
    *
    * @param documentB64: the document zipped and encoded
    * @param validator:   the validator to use
    * @param mediaType:   the mediatype of the result of validation
    *
    * @return the detailed result as forwarded by the validator
    */
   public String validateZipAndReturnXMLString(String documentB64, String validator, String mediaType) {
      String resAsB64Zipped = validateZipAndReturnB64Zip(documentB64, validator, mediaType);
      if (resAsB64Zipped != null) {
         byte[] reszip = DatatypeConverter.parseBase64Binary(resAsB64Zipped);
         ByteArrayOutputStream baosres = new ByteArrayOutputStream();
         try (ByteArrayInputStream ba = new ByteArrayInputStream(reszip)) {
            unzip(ba, baosres);
            return baosres.toString();
         } catch (IOException e) {
            log.error("Not able to unzip", e.getMessage());
         }
      }
      return null;
   }

   public String zipAndvalidateXMLAndReturnXMLString(byte[] documentXML, String validator, String mediaType) {
      if (documentXML != null) {
         ByteArrayOutputStream baos = new ByteArrayOutputStream();
         try (ByteArrayInputStream ba = new ByteArrayInputStream(documentXML)) {
            zip(ba, baos);
            String b64 = DatatypeConverter.printBase64Binary(baos.toByteArray());
            return validateZipAndReturnXMLString(b64, validator, mediaType);
         } catch (IOException e) {
            log.error("Not able to zip", e.getMessage());
         }
      }
      return null;
   }

   public static void zip(InputStream in, OutputStream os) throws IOException {
      if (os != null && in != null) {
         byte[] buffer = new byte[8192];
         int read = 0;
         try (ZipOutputStream zos = new ZipOutputStream(os);) {
            ZipEntry entry = new ZipEntry("mbvalResult.xml");
            zos.putNextEntry(entry);
            while (-1 != (read = in.read(buffer))) {
               zos.write(buffer, 0, read);
            }
            in.close();
         }
      }
   }

   private void unzip(InputStream is, OutputStream os) throws IOException {
      try (ZipInputStream archive = new ZipInputStream(is)) {
         ZipEntry entry;
         while ((entry = archive.getNextEntry()) != null) {
            log.info("entry: " + entry.getName() + ", " + entry.getSize());
            BufferedOutputStream out = new BufferedOutputStream(os);

            byte[] buffer = new byte[8192];
            int read;
            while (-1 != (read = archive.read(buffer))) {
               out.write(buffer, 0, read);
            }
            out.close();
         }
      }

   }

   public long getTimeout() {
      return timeout;
   }

   public void setTimeout(long timeout) {
      this.timeout = timeout;
   }
}
