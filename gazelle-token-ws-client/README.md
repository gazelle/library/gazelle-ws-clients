# Gazelle Token WS Client

Secure web service in Gazelle applications using users' API keys from Gazelle Token Service.

Add as dependencies in your project

```xml
<dependency>
    <groupId>net.ihe.gazelle.modules</groupId>
    <artifactId>gazelle-token-ws-client</artifactId>
    <version>${token.wsclient.version}</version>
</dependency>
```

## To protect a webservice


Add the `TokenAuthenticationFilter` in the `web.xml` and filter resources that need to be protected.

```xml

<web-app>
    ...
    <filter>
        <filter-name>Token API Authentication Filter</filter-name>
        <filter-class>net.ihe.gazelle.token.wsclient.GazelleApiKeyAuthenticationFilternet.ihe.gazelle.token.wsclient.GazelleApiKeyAuthenticationFilter</filter-class>
    </filter>
    <filter-mapping>
        <filter-name>Token API Authentication Filter</filter-name>
        <url-pattern>/rest/resource1*</url-pattern>
        <url-pattern>/rest/resource2*</url-pattern>
        <url-pattern>...</url-pattern>
    </filter-mapping>
    ...
</web-app>
```

Also add the `TokenAuthenticationFilter` in the `components.xml`

```xml

<components>
    ...
    <component name="org.jboss.seam.web.authenticationFilter"
               class="net.ihe.gazelle.token.wsclient.GazelleApiKeyAuthenticationFilter"
               jndi-name="java:app/your-module-name/GazelleApiKeyAuthenticationFilter">
        <property name="urlPattern">/rest/resource1*</property>
        <property name="urlPattern">/rest/resource2*</property>
        <property name="urlPattern">...</property>
    </component>
    <web:rewrite-filter view-mapping="/rest/resource1*"/>
    <web:rewrite-filter view-mapping="/rest/resource2*"/>
    <web:rewrite-filter view-mapping="..."/>
    ...
</components>
```

The filter also requires a Java SPI implementation of `PreferenceProvider` from gazelle-preferences v7 to
retrieve the preference `token_api_url`.

```java
@MetaInfServices(PreferenceProvider.class)
public class PreferenceProviderImpl implements PreferenceProvider {
   //...
}
```

`token_api-url` must have the URL of the token service.

## To get information about the authentified user

In your web service controler, access the authentified user from the servlet attribute `TokenAuthenticationFilter.CONST_TOKEN_ASSERTION`:

```java
@Path("/resource1")
class MyController {

   @GET
   public Response getResource1(@Context HttpServletRequest servletRequest) {
      TokenIdentity identity = 
            (TokenIdentity) servletRequest.getAttribute(TokenAuthenticationFilter.CONST_TOKEN_ASSERTION);
      if(identity.isLoggedIn() && identity.hasRole("admin_role")) {
         LOG.debug(identity.getUsername());
         //...
      }
   }
}
```

`TokenIdentity` can also be cast as `GazelleIdentity` interface (the default one from SSO API).
