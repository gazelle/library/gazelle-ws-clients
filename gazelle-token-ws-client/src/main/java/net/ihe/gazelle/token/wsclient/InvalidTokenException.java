package net.ihe.gazelle.token.wsclient;

public class InvalidTokenException extends Exception {
    public InvalidTokenException(String s) {
        super(s);
    }
}
