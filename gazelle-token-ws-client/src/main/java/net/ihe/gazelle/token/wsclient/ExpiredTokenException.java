package net.ihe.gazelle.token.wsclient;

public class ExpiredTokenException extends Exception {
    public ExpiredTokenException(String s) {
        super(s);
    }
}
