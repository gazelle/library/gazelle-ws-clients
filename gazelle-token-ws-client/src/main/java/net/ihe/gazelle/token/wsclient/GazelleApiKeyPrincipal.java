package net.ihe.gazelle.token.wsclient;

import com.fasterxml.jackson.databind.JsonNode;

import java.io.Serializable;
import java.security.Principal;
import java.util.Objects;

public class GazelleApiKeyPrincipal implements Principal, Serializable {

    private final JsonNode gazelleApiKey;

    public GazelleApiKeyPrincipal(JsonNode gazelleApiKey) {
        this.gazelleApiKey = gazelleApiKey;
    }

    @Override
    public String getName() {
        return gazelleApiKey.textValue();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        GazelleApiKeyPrincipal that = (GazelleApiKeyPrincipal) o;
        return Objects.equals(gazelleApiKey, that.gazelleApiKey);
    }

    @Override
    public int hashCode() {
        return Objects.hash(gazelleApiKey);
    }
}
